$(document).ready(() => {
  $(".deleteUser").on("click", deleteUser);
});

function deleteUser() {
  var confirmation = confirm("Are you sure?");
  if (confirmation) {
    let url = "/users/delete/" + $(this).data("id");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      let status = xhttp.status;
      let readyState = xhttp.readyState;
      if (readyState == 4 && status == 200) {
        location.replace("/");
      }
    };
    xhttp.open("DELETE", url);
    xhttp.send();
  } else {
    return false;
  }
}
