const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const expressValidator = require("express-validator");
const mongojs = require("mongojs");
const ObjectId = mongojs.ObjectId;
const db = mongojs("customerapp", ["users"]);

const app = express();

// View engine
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

// Bodyparser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Set static path
app.use(express.static(path.join(__dirname, "public")));

// Global Vars
app.use(function(req, res, next) {
  res.locals.errors = null;
  next();
});

// Express validator middleware
app.use(expressValidator());

app.get("/", (req, res) => {
  db.users.find((err, docs) => {
    res.render("index", { title: "Customers", users: docs });
  });
});

app.post("/users/add", (req, res) => {
  req.checkBody("first_name", "First name is required").notEmpty();
  req.checkBody("last_name", "Last name is required").notEmpty();
  req.checkBody("email", "Email is required").notEmpty();

  const errors = req.validationErrors();

  if (errors) {
    res.render("index", { title: "Customers", users: users, errors: errors });
    console.log("Errors");
  } else {
    const { first_name, last_name, email } = req.body;

    const newUser = {
      first_name,
      last_name,
      email
    };
    db.users.insert(newUser, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.redirect("/");
      }
    });
  }
});

app.delete("/users/delete/:id", (req, res) => {
  db.users.remove({ _id: ObjectId(req.params.id) }, (err, result) => {
    if (err) {
      console.log(err);
    }
    res.status(200).send();
  });
});

app.listen(3000, function() {
  console.log("Server started in Port 3000");
});
